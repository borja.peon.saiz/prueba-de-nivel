<?php
    require __DIR__ . "/inc/root_path.php";
    require_once PROJECT_ROOT_PATH . "/Model/Database.php";

// echo "<pre>";
// print_r($response);
// echo "</pre>";

    // Cargamos el array de departamentos para mostrarlo en el select
    $arrayDepartamentos = [];
    $query = "SELECT * FROM `departments`";
    $database = new Database();
    $arrayDepartamentos = $database->select($query);
    $options = "<option value=''>Elegir</option>";

    //construye las opciones del select
    foreach ($arrayDepartamentos as $key => $value) {
        $options = $options."<option value='".$value["dept_no"]."'>".$value["dept_name"]."</option>";
    }

?>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <title>Borja Peon Saiz</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  </head>
    <body>
        <div class="container">
            <div class="d-flex flex-row align-items-center">
                <div class="col-10 p-3">
                    <h1>Empleados</h1>
                </div>
                <div class="col-2 p-3 ">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#añadir">
                    Crear empleado
                </button>
                </div>
            </div>
            <div class="d-flex flex-row align-items-center m-2 p-2 border border-dark bg-info">
                <div class="col-1">
                    <p class="m-0 p-0 center">Nº</p>
                </div>
                <div class="col-2">
                    <p class="m-0 p-0">Nombre</p>
                </div>
                <div class="col-3">
                    <p class="m-0 p-0">Apellidos</p>
                </div>
                <div class="col-2">
                    <p class="m-0 p-0">Departamento</p>
                </div>
                <div class="col-2">
                    <p class="m-0 p-0">Titulación</p>
                </div>
                <div class="col-1">
                    <p class="m-0 p-0">Sueldo</p>
                </div>
                <div class="col-1">
                    <p class="m-0 p-0">Fecha inicio</p>
                </div>
            </div>
            <div class="row p-2 m-2" id='list'>

            </div>       
        </div>
        <!-- Modal para insertar nuevos empleados -->
        <div class="modal fade" id="añadir">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Añadir Empleado</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row align-items-center m-2 p-2 border border-dark">
                            <div class="col-3">
                                <p class="m-0 p-0">Nombre</p>
                                <p><input id='nombre' type="text" class="form-control" placeholder="Nombre" aria-label="Nombre"  required></p>
                            </div>
                            <div class="col-3">
                                <p class="m-0 p-0">Apellidos</p>
                                <p><input id='apellidos' type="text" class="form-control" placeholder="Apellidos" aria-label="Apellidos" aria-describedby="basic-addon1 required"></p>
                            </div>
                            <div class="col-3">
                                <p class="m-0 p-0">Fecha de nacimiento</p>
                                <p><input id='nacimiento' type="date" name="nacimiento" required></p>
                            </div>
                            <div class="col-3">
                                <p class="m-0 p-0">Genero</p>
                                <p>
                                <select id='genero' class="form-select" aria-label="genero" required>
                                    <option selected>Elegir</option>
                                    <option value="M">Hombre</option>
                                    <option value="F">Mujer</option>
                                </select>
                                </p>
                            </div>
                            <div class="col-4">
                                <p class="m-0 p-0">Departamento</p>
                                <p>
                                    <select id='departamento' class="form-select" aria-label="departamento" required>
                                        <?= $options ?>
                                    </select>
                                </p>
                            </div>
                            <div class="col-4">
                                <p class="m-0 p-0">Titulación</p>
                                <p><input id='titulacion' type="text" class="form-control" placeholder="Titulación" aria-label="titulacion" required ></p>
                            </div>
                            <div class="col-4">
                                <p class="m-0 p-0">Sueldo</p>
                                <p><input id='sueldo' type="number" class="form-control" placeholder="Sueldo" aria-label="sueldo" required ></p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id='guardar' onclick="insertEmployee()" data-bs-dismiss="modal">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
     
    <!-- Si utilizamos componentes de Bootstrap que requieran Javascript agregar el siguiente archivo -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    </body>
</html>

<script>
    $( document ).ready(function() {
        //llamada ajax al listado
        ajaxListEmployee();
    });

    // funcion que llama por ajax a la api para insertar el empleado
    function insertEmployee(){
        $.ajax({
            url: "/View/insertEmployee.php",
            data: {
                nombre: $('#nombre').val(),
                apellidos: $('#apellidos').val(),
                nacimiento: $('#nacimiento').val(),
                genero: $('#genero').val(),
                departamento: $('#departamento').val(),
                titulacion: $('#titulacion').val(),
                sueldo: $('#sueldo').val()
            },
            success: function( result ) {
                if(result != ''){
                    ajaxListEmployee();
                    //Aqui deberiamos coger el valor KO / OK y lanzar un mensaje indicando si hay algun error
                }
            }
        });
    }
    // funcion que llama por ajax a la api para listar los empleados
    function ajaxListEmployee(){
        $.ajax({
            url: "/View/listEmployee.php",
            success: function( result ) {
                if(result != ''){
                    $("#list").html(result);
                }
            }
        });
    }
</script>
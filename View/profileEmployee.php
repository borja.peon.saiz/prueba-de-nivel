<?php 
    $response = [];
    //comprobamos la llegada de datos
    if(isset($_GET['employee_id'])){

        $id_employee = $_GET['employee_id'];

        //cargamos la url del API
        $url = 'http://localhost/ApiRest.php/employee/'.$_GET['employee_id'].'/profile?limit=50';    
        
        //Ejecutamos CURL
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        // Optenemos la respuesta
        $response = json_decode($response);

        // Devolvemos los datos del perfil
        foreach ($response as $key => $employee) {
            echo "<div class='col-3 '><p>Nombre</p><p>".$employee->first_name."</p></div>";
            echo "<div class='col-3 '><p>Apellido</p><p>".$employee->last_name."</p></div>";
            echo "<div class='col-3 '><p>Genero</p><p>".$employee->gender."</p></div>";
            echo "<div class='col-3 '><p>Fecha de nacimiento</p><p>".$employee->birth_date."</p></div>";
            echo "<div class='divider m-2 border-bottom border-danger'></div>";
            echo "<div class='col-4 '><p>Departamento</p><p>".$employee->dept_name."</p></div>";
            echo "<div class='col-2 '><p>Titulo</p><p>".$employee->title."</p></div>";
            echo "<div class='col-2 '><p>Salario</p><p>".$employee->salary."</p></div>";
            echo "<div class='col-4 '><p>Fecha de contrato</p><p>".$employee->hire_date."</p></div>";
        }
    }
?>
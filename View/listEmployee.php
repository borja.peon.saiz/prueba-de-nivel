<?php 
    $resp = [];

    //cargamos la url del API
    $url = 'http://localhost/ApiRest.php/employee/list?limit=50';    

    //Ejecutamos CURL
    $curl = curl_init();
    
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    
    $resp = curl_exec($curl);
    
    curl_close($curl);

    // Optenemos la respuesta
    $resp = json_decode($resp);
    
    // Devolvemos los datos del listado
    foreach ($resp as $key => $employee) {
        echo "<div class='employee d-flex flex-row align-items-center mb-2 p-2 border border-dark bg-secondary ' style='color:white; cursor:pointer;' id='".$employee->emp_no."'>";
        echo "<div class='col-1'><p class='m-0 p-0'>".$employee->emp_no."</p></div>";
        echo "<div class='col-2'><p class='m-0 p-0'>".$employee->first_name."</p></div>";
        echo "<div class='col-3'><p class='m-0 p-0'>".$employee->last_name."</p></div>";
        echo "<div class='col-2'><p class='m-0 p-0'>".$employee->dept_name."</p></div>";
        echo "<div class='col-2'><p class='m-0 p-0'>".$employee->title."</p></div>";
        echo "<div class='col-1'><p class='m-0 p-0'>".$employee->salary."</p></div>";
        echo "<div class='col-1'><p class='m-0 p-0'>".$employee->hire_date."</p></div>";
        echo "</div>";
        echo "<div class='row' id='profile".$employee->emp_no."' ></div>";
    }
?>

<script>
    // Se ejecuta la llamada al API para mostrar el perfil
    $('.employee').click(function() {
        if($("#profile"+$(this).attr('id')).html() != ''){
            $("#profile"+$(this).attr('id')).html('');
        }else{
            ajaxProfileEmployee($(this).attr('id'));
        }
        
    });

    // Funcion que llama por ajax a la API para mostrar el perfil
    function ajaxProfileEmployee(employee_id){
        $.ajax({
            url: "/View/profileEmployee.php",
            data: {
                employee_id: employee_id
            },
            success: function( result ) {
                console.log(result);
                if(result != ''){
                    $("#profile"+employee_id).html(result);
                }
            }
        });
    }

</script>
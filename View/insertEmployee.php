<?php 
    $mensaje = "";
    //comprobamos la llegada de datos
    if((isset($_GET['nombre']) && $_GET['nombre'] != '') 
        && (isset($_GET['apellidos']) && $_GET['apellidos'] != '')
        && (isset($_GET['nacimiento']) && $_GET['nacimiento'] != '')
        && (isset($_GET['genero']) && $_GET['genero'] != '')
        && (isset($_GET['departamento']) && $_GET['departamento'] != '')
        && (isset($_GET['sueldo']) && $_GET['sueldo'] != '')
        && (isset($_GET['titulacion']) && $_GET['titulacion'] != '')
    ){
        $resp = [];
        //cargamos la url del API
        $url = 'http://localhost/ApiRest.php/employee/insert';    
        
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_URL, $url);

        $data = array(
            'nombre' => $_GET['nombre'],
            'apellidos' => $_GET['apellidos'],
            'nacimiento' => $_GET['nacimiento'],
            'genero' => $_GET['genero'],
            'departamento' => $_GET['departamento'],
            'titulacion' => $_GET['titulacion'],
            'sueldo' => $_GET['sueldo']
        );

        $payload = json_encode($data);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        
        $resp = curl_exec($curl);
        
        curl_close($curl);
        // Optenemos la respuesta
        $resp = json_decode($resp);

        if($resp == 1){
            $mensaje = " El empleado a sido registrado.";
        }else{
            $mensaje = " Error, el empleado no a podido ser registrado.";
        }

        
    }else{
        $mensaje = "Es necesario introducir todos los datos.";
    } 
    // Devolvemos mensaje OK / KO
    echo $mensaje;
?>
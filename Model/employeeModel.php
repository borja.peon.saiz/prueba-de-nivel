<?php
require_once PROJECT_ROOT_PATH . "/Model/Database.php";
 
class employeeModel extends Database
{
    // Funcion que trae de bbdd el listado
    public function getEmployees($limit)
    {
        $query = "SELECT em.emp_no, em.first_name, em.last_name, dept.dept_name, ti.title, sa.salary, em.hire_date
        FROM employees as em 
        JOIN dept_emp as de on (em.emp_no = de.emp_no)
        JOIN departments as dept on (de.dept_no = dept.dept_no)
        JOIN titles as ti on (ti.emp_no = em.emp_no)
        JOIN salaries as sa on (sa.emp_no = ti.emp_no)
        WHERE sa.to_date = (SELECT MAX( sa.to_date )  FROM salaries where salaries.emp_no = em.emp_no) 
        AND de.to_date = (SELECT MAX( sa.to_date )  FROM dept_emp where dept_emp.emp_no = em.emp_no) 
        AND ti.to_date = (SELECT MAX( sa.to_date )  FROM titles where titles.emp_no = em.emp_no)
        ORDER BY emp_no ASC LIMIT ".$limit;
        return $this->select($query);
    }

    // Funcion que trae de bbdd el perfil segun un ID
    public function getEmployeesProfile($limit, $emp_no)
    {
        $query = "SELECT em.emp_no, em.first_name, em.last_name, em.gender, dept.dept_name, ti.title, sa.salary, em.hire_date, em.birth_date
        FROM employees as em 
        JOIN dept_emp as de on (em.emp_no = de.emp_no)
        JOIN departments as dept on (de.dept_no = dept.dept_no)
        JOIN titles as ti on (ti.emp_no = em.emp_no)
        JOIN salaries as sa on (sa.emp_no = ti.emp_no)
        WHERE sa.to_date = (SELECT MAX( sa.to_date )  FROM salaries where salaries.emp_no = em.emp_no) 
        AND de.to_date = (SELECT MAX( sa.to_date )  FROM dept_emp where dept_emp.emp_no = em.emp_no) 
        AND ti.to_date = (SELECT MAX( sa.to_date )  FROM titles where titles.emp_no = em.emp_no)
        AND em.emp_no = '".$emp_no."'
        ORDER BY emp_no ASC LIMIT ".$limit;
        return $this->select($query);
    }

    // Funcion que graba nuevo empleado
    public function insertEmployees($data)
    {
        $ID = [];
        $grabado = true;
        $queryUltimoId = 'SELECT MAX(emp_no) AS id FROM employees';
        $ID = $this->select($queryUltimoId);

        $insertID = $ID[0]['id'] + 1;

        // Solo se graba en una tabla si se ha registrado
        $query = "INSERT INTO employees (emp_no, birth_date, first_name, last_name, gender, hire_date) 
                  VALUES ('".$insertID."','".$data['nacimiento']."','".$data['nombre']."','".$data['apellidos']."','".$data['genero']."',Now());";
        if(!$this->ejecutar($query)){
            $grabado = false;
        }else{
            $query2 = "INSERT INTO dept_emp (emp_no, dept_no,from_date,to_date) 
            VALUES ('".$insertID."','".$data['departamento']."', Now(),'9999-01-01');";
            if(!$this->ejecutar($query2)){
                $grabado = false;
                // aqui deberiamos eliminar el registro de employees
            }else{
                $query3 = "INSERT INTO salaries (emp_no, salary,from_date,to_date) 
                VALUES ('".$insertID."','".$data['sueldo']."', Now(),'9999-01-01');";
                if(!$this->ejecutar($query3)){
                    $grabado = false;
                    // aqui deberiamos eliminar el registro de employees
                    // aqui deberiamos dept_emp el registro de employees
                }else{
                    $query4 = "INSERT INTO titles (emp_no, title, from_date, to_date) 
                    VALUES ('".$insertID."','".$data['titulacion']."', Now(),'9999-01-01');";
                    if($this->ejecutar($query4) === false){
                        $grabado = false;
                        // aqui deberiamos eliminar el registro de employees
                        // aqui deberiamos dept_emp el registro de employees
                        // aqui deberiamos salaries el registro de employees
                    }
                }
            }
        }
        return $grabado;
    }
}
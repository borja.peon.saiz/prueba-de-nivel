<?php
class Database
{
    protected $connection = null;
 
    // constructor para conexion a bbdd
    public function __construct()
    {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
         
            if ( mysqli_connect_errno()) {
                throw new Exception("Could not connect to database.");   
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());   
        }           
    }
 
    // Funcion para buscar en bbdd
    public function select($query = "")
    {
        try {
            $stmt = $this->executeStatement($query);
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);               
            $stmt->close();
 
            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return false;
    }

    // Funcion para ejecutar insert, delete o updates en bbdd
    public function ejecutar($query = "")
    {
        try {
            $stmt = $this->executeStatement($query); 
            if($stmt){
                $stmt->close();
                return 1;
            } else{
                $stmt->close();
                return 0;
            }            

        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
            return 0;
        }
    }
    
    //Funcion que ejecuta la llamada a bbdd
    private function executeStatement($query = "")
    {
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            $stmt->execute();
 
            return $stmt;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }   
    }
}
<?php
require __DIR__ . "/inc/root_path.php";
require PROJECT_ROOT_PATH . "/Controller/Api/EmployeeController.php";

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

// comprobamos que la url trae los parametros necesarios
if ((isset($uri[2]) && $uri[2] != 'employee') || !isset($uri[3])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

//con los valores de la url lanzamos las acciones correspondientes
if (isset($uri[4])){
    $EmployeeController = new EmployeeController();
    $action = $uri[4] . 'Action';
    $EmployeeController->{$action}($uri[3]);
}else{
    $EmployeeController = new EmployeeController();
    $action = $uri[3] . 'Action';
    $EmployeeController->{$action}();
}
?>